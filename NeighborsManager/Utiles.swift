//
//  Utiles.swift
//  NeighborsManager
//
//  Created by LinuxPlus on 8/26/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit
import MapKit


class Utiles{
    
    static func showAlert(title:String ,message:String) -> UIAlertController{
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        return alert
    }
    
    static func generateRandomMarkers(numberOfMarkers total: Int) -> [MarkerPoint] {
        var markers:[MarkerPoint] = [MarkerPoint]()
        
        for i in 0..<total{
            let randomLatitude = drand48()  + 29.0001
            let randomLongitude = drand48() + 30.0001
            let coordinate = CLLocationCoordinate2D(latitude: randomLatitude, longitude: randomLongitude)
            let title = "Car \(i + 1)"
            let subtitle = "Car Location"
            let image:UIImage? = (i != 1) ? UIImage(named: "carIcon") : nil
            let color:UIColor? = (image == nil) ? UIColor.blue : nil
            
            let marker = MarkerPoint(coordinate:coordinate)
            marker.title = title
            marker.subtitle = subtitle
            marker.image = image
            marker.color = color

            markers.append(marker)
        }
        return markers
    }
    
    static func displayMapWithRadius(mapView:MKMapView, mapCenter:CLLocationCoordinate2D ,mapRadius:CLLocationDistance){
        let region = MKCoordinateRegionMakeWithDistance(mapCenter, mapRadius, mapRadius)
        mapView.setRegion(region, animated: true)
    }

}
