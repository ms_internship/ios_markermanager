//
//  PointDrawerDelegate.swift
//  NeighborsManager
//
//  Created by LinuxPlus on 8/26/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import MapKit

protocol PointDrawerDelegate{
    func pointDrawer(markerTapped marker:MarkerPoint)
}
