//
//  RegionMonitorProtocol.swift
//  LocationMarkers
//
//  Created by LinuxPlus on 8/23/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

protocol RegionMonitorProtocol {
    var delegate: RegionMonitorDelegate? {get set}
    
    func getRegion(mapCenter: CLLocationCoordinate2D, mapRadius: CLLocationDistance) -> CLCircularRegion
    func updateCenter(newCenter center: CLLocationCoordinate2D)
    func isRegionContainsMarker(marker: MarkerPoint) -> Bool
    func getInsideRegionMarkers(markers: [MarkerPoint]) -> [MarkerPoint]
    func updateMarkerState(inRegionMarkers: [MarkerPoint], marker: MarkerPoint)
}
