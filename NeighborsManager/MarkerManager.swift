//
//  MarkerManager.swift
//  LocationMarkers
//
//  Created by LinuxPlus on 8/21/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

class MarkerManager:NSObject {
    var delegate: MarkerManagerDelegate?
    
    fileprivate var minimumDistance:CLLocationDistance?
    fileprivate var minimumTime: Double?
    
    fileprivate var markers:[MarkerPoint]?
    fileprivate var insideRegionMarkers = [MarkerPoint]()
    fileprivate var regionMonitor: RegionMonitorProtocol!
    fileprivate var pointDrawer: PointDrawerProtocol!
    
    
    init?(builder: MarkerManagerBuilder){
        super.init()
        if let mapView = builder.mapView, let mapRadius = builder.mapRadius, let mapCenter = builder.mapCenter{
            pointDrawer = PointDrawer(map: mapView)
            regionMonitor = RegionMonitor(center: mapCenter, radius: mapRadius)
        }else{
            return nil
        }
        self.markers = builder.markers
        
        if let minimumDistance = builder.minimumDistance{
            self.minimumDistance = minimumDistance
        }else{
            minimumDistance = 100 // 100 meter
        }
        
        if let minimumTime = builder.minimumTime{
            self.minimumTime = minimumTime
        }else{
            minimumTime = 5000 // 5 seconds
        }
        
        pointDrawer.delegate = self
        regionMonitor.delegate = self
    }
    
}


extension MarkerManager: MarkerManagerProtocol{
    
    func setMarkers(){
        if let markers = self.markers{
            setMarkers(markers: markers)
        }
    }
    
    func setMarkers(markers: [MarkerPoint]){
        self.insideRegionMarkers = regionMonitor.getInsideRegionMarkers(markers: markers)
        pointDrawer.setMarkers(markerPoints: insideRegionMarkers)
    }
    
    func addMarkers(appendMarkers markers:[MarkerPoint]){
        self.insideRegionMarkers.append(contentsOf: regionMonitor.getInsideRegionMarkers(markers: markers))
        pointDrawer.addMarkers(appendMarkers: insideRegionMarkers)
    }
    
    
    func addMarker(appendMarker marker:MarkerPoint){
        if regionMonitor.isRegionContainsMarker(marker: marker){
            insideRegionMarkers.append(marker)
            pointDrawer.addMarker(appendMarker: marker)
        }
    }
    
    
    func updateMarker(newMarker:MarkerPoint){
        if isTimeAndDistanceValid(newMarker: newMarker){
            UIView.animate(withDuration: 0.5) {
                newMarker.lastTimeUpdated = Date().timeIntervalSince1970
                newMarker.previousLocation = newMarker.coordinate
                self.regionMonitor.updateMarkerState(inRegionMarkers: self.insideRegionMarkers, marker: newMarker)
            }
        }
    }
    
    func updateCenter(newCenter center: CLLocation) {
        regionMonitor.updateCenter(newCenter: center.coordinate)
        markers?.forEach({ (marker) in
            updateMarker(newMarker: marker)
        })
    }
    
    func removeAllMarkers(){
        pointDrawer.removeAllMarkers()
    }
    func removeMarker(markerPoint :MarkerPoint){
        pointDrawer.removeMarker(markerPoint: markerPoint)
    }
    
    private func isTimeAndDistanceValid(newMarker:MarkerPoint) -> Bool{
        let index = markers?.index(of: newMarker)
        guard index != nil else {
            return false
        }
        let oldMarker = markers![index!]
        let time = Date().timeIntervalSince1970 - oldMarker.lastTimeUpdated!
        let distance = newMarker.distanceMoved()
        return (time >= minimumTime! || distance >= minimumDistance!)
    }
    
}

extension MarkerManager: PointDrawerDelegate{
    func pointDrawer(markerTapped marker: MarkerPoint) {
        delegate?.markerManager(markerTapped: marker)
    }
}

extension MarkerManager: RegionMonitorDelegate{
    
    func regionMonitor(didUpdateRegion region: CLCircularRegion) {
        delegate?.markerManager(didUpdateRegion: region)
    }
    
    func regionMonitor(didEnterRegion marker: MarkerPoint){
        insideRegionMarkers.append(marker)
        pointDrawer.addMarker(appendMarker: marker)
        delegate?.markerManager(didEnterRegion: marker)
    }
    
    func regionMonitor(didExitRegion marker: MarkerPoint){
        if let index = insideRegionMarkers.index(of: marker){
            insideRegionMarkers.remove(at: index)
            pointDrawer.removeMarker(markerPoint: marker)
        }
        delegate?.markerManager(didExitRegion: marker)
    }
    
    func regionMonitor(stillInRegion marker: MarkerPoint) {
        pointDrawer.updateMarker(newMarker: marker)
    }
}


