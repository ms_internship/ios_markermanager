//
//  LocationUpdatesDelegate.swift
//  GeoLocation
//
//  Created by LinuxPlus on 8/16/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import CoreLocation

public protocol LocationUpdatesDelegate {
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager)
    func locationUpdates(didUpdateLocation location: CLLocation)
    func locationUpdates(didFailWithError error: Error)

}

extension LocationUpdatesDelegate{
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager){}
    func locationUpdates(didFailWithError error: Error){}
}
