//
//  MarkerManagerDelegate.swift
//  LocationMarkers
//
//  Created by LinuxPlus on 8/24/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import CoreLocation

protocol MarkerManagerDelegate {
    
    func markerManager(didEnterRegion marker: MarkerPoint)
    func markerManager(didExitRegion marker: MarkerPoint)
    func markerManager(markerTapped marker:MarkerPoint)
    func markerManager(didUpdateRegion region: CLCircularRegion)

}

extension MarkerManagerDelegate{
    func markerManager(didUpdateRegion region: CLCircularRegion){}
    func markerManager(markerTapped marker:MarkerPoint){}
}
