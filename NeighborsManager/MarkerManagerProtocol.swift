//
//  MarkerManagerProtocol.swift
//  LocationMarkers
//
//  Created by LinuxPlus on 8/21/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import MapKit


protocol MarkerManagerProtocol {
    var delegate: MarkerManagerDelegate? {get set}
    
    func setMarkers()
    func setMarkers(markers: [MarkerPoint])
    func addMarkers(appendMarkers markers: [MarkerPoint])
    func addMarker(appendMarker marker:MarkerPoint)
    func updateMarker(newMarker:MarkerPoint)
    func updateCenter(newCenter center: CLLocation)
    func removeAllMarkers()
    func removeMarker(markerPoint :MarkerPoint)
}
