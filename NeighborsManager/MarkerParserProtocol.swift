//
//  MarkerParserProtocol.swift
//  NeighborsManager
//
//  Created by LinuxPlus on 8/26/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation

protocol MarkerParserProtocol{
    static var shared:MarkerParser {get set}
    func parseMarkerPoint<T>(markerPoint:MarkerPoint) -> T
    func parseMarkerPoints<T>(markerPoints:[MarkerPoint]) -> [T]
    func getMarkerPoint<T>(marker:T) -> MarkerPoint?
}
