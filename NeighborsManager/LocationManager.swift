//
//  LocationManager.swift
//  GeoLocation
//
//  Created by LinuxPlus on 8/20/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager{
    
    var delegate: LocationManagerDelegate?
    
    var locationAuth: LocationAuthorizationProtocol = LocationServiceAuthorization()
    var locationUpdate: LocationUpdatesProtocol = LocationUpdates()
    
    init() {
        locationAuth.locationAuthorizationDelegate = self
        locationUpdate.locationUpdatesDelegate = self
    }
    
}



extension LocationManager: LocationManagerProtocol{
    func isLocationServiceAuthorized() -> Bool{
        return locationAuth.isLocationServiceAuthorized()
    }
    
    func startTrackingUserLocation(){
        if locationAuth.isLocationServiceEnabled(){
            if locationAuth.isLocationServiceAuthorized(){
                locationUpdate.startUpdatingLocation()
            }else{
                locationAuth.requestAlwaysAuthorization()
            }
        }
    }
    
    
    func stopTrackingUserLocation(){
        locationUpdate.startUpdatingLocation()
    }
    
    func requestAlwaysAuthorization(){
        locationAuth.requestAlwaysAuthorization()
    }
    
    func requestWhenInUseAuthorization(){
        locationAuth.requestWhenInUseAuthorization()
    }
    
}

extension LocationManager: LocationAuthorizationDelegate{
    func locationAuthorizationFailed(){
        delegate?.locationAuthorizationFailed()
    }
    func locationAuthorizationSuccesfull(){
        delegate?.locationAuthorizationSuccesfull()
        startTrackingUserLocation()
    }
}

extension LocationManager: LocationUpdatesDelegate{
    
    func locationUpdates(didUpdateLocation location: CLLocation){
        delegate?.locationManager(didUpdateLocation: location)
    }
    func locationUpdates(didFailWithError error: Error){
        delegate?.locationManager(didFailWithError: error)
    }
    
}


