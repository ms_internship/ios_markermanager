//
//  MapKitMarker.swift
//  NeighborsManager
//
//  Created by LinuxPlus on 8/27/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class MapKitAnnotation:NSObject, MKAnnotation{
    var identifier:String
    dynamic var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var image: UIImage?
    var color: UIColor?
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
        identifier = UUID().uuidString
    }
    
}
