//
//  LocationAutorizationDelegate.swift
//  GeoLocation
//
//  Created by LinuxPlus on 8/16/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation

public protocol LocationAuthorizationDelegate {
    func locationManagerIsNotEnabled()
    func locationAuthorizationFailed()
    func locationAuthorizationSuccesfull()
    
}

extension LocationAuthorizationDelegate{
    func locationManagerIsNotEnabled(){}
    func locationAuthorizationFailed(){}
    func locationAuthorizationSuccesfull(){}
    
}
