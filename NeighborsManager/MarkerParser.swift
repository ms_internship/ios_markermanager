//
//  MarkerParser.swift
//  NeighborsManager
//
//  Created by LinuxPlus on 8/26/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import MapKit

class MarkerParser: MarkerParserProtocol {
    
    static var shared: MarkerParser = MarkerParser()
    fileprivate var allMapMarkers: [MarkerPoint:MapKitAnnotation] = [:]

    private init(){
        
    }
    
    func parseMarkerPoint<T>(markerPoint:MarkerPoint) -> T{
        
        if let oldMarker = allMapMarkers[markerPoint]{
            return oldMarker as! T
        }
        
        let annotation = MapKitAnnotation(coordinate: markerPoint.coordinate)
        annotation.coordinate = markerPoint.coordinate
        annotation.title = markerPoint.title
        annotation.subtitle = markerPoint.subtitle
        annotation.image = markerPoint.image
        annotation.color = markerPoint.color
        
        allMapMarkers[markerPoint] = annotation
        return annotation as! T
    }
    
    func parseMarkerPoints<T>(markerPoints:[MarkerPoint]) -> [T]{
        var markers:[MapKitAnnotation] = [MapKitAnnotation]()
        
        for point in markerPoints{
            let marker:MapKitAnnotation = parseMarkerPoint(markerPoint: point)
            markers.append(marker)
            allMapMarkers[point] = marker
        }
        return markers as! [T]
    }
    
    func getMarkerPoint<T>(marker:T) -> MarkerPoint?{
        guard marker is MapKitAnnotation else {
            return nil
        }
        
        let annotation = marker as! MapKitAnnotation
        let markerPoint = MarkerPoint(coordinate: annotation.coordinate)
        markerPoint.title = annotation.title
        markerPoint.subtitle = annotation.subtitle
        markerPoint.image = annotation.image
        markerPoint.color = annotation.color
        return markerPoint
}


}

