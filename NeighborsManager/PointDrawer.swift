//
//  PointDrawer.swift
//  LocationMarkers
//
//  Created by LinuxPlus on 8/24/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

class PointDrawer: NSObject {
    var delegate: PointDrawerDelegate?
    fileprivate var mapView: MKMapView?
    fileprivate let markerParser = MarkerParser.shared
    
    init(map: MKMapView){
        super.init()
        self.mapView = map
        self.mapView?.delegate = self
    }
}


extension PointDrawer: MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? MapKitAnnotation{
            
            let reuseIdentifier = annotation.identifier
            
            if let dequedView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier){
                dequedView.annotation = annotation
                return dequedView
            }else{
                return createNewMarkerView(annotation: annotation)
            }
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if let annotation = markerParser.getMarkerPoint(marker: view.annotation){
            delegate?.pointDrawer(markerTapped: annotation)
        }
    }
    
    
    private func createNewMarkerView(annotation: MapKitAnnotation) -> MKAnnotationView{
        let reuseIdentifier = annotation.identifier

        if let image = annotation.image{
            let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView.image = image
            annotationView.canShowCallout = true
            annotationView.rightCalloutAccessoryView = UIButton(type: .infoLight)
            return annotationView
        }else{
            let annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView.pinTintColor = annotation.color ?? UIColor.red
            annotationView.canShowCallout = true
            annotationView.rightCalloutAccessoryView = UIButton(type: .infoLight)
            return annotationView
        }
        
    }
    
    
}

extension PointDrawer: PointDrawerProtocol{
    
    func setMarkers(markerPoints: [MarkerPoint]){
        let markers:[MapKitAnnotation] = markerParser.parseMarkerPoints(markerPoints: markerPoints)
        removeAllMarkers()
        mapView?.addAnnotations(markers)
    }
    
    func addMarker(appendMarker markerPoint: MarkerPoint){
        let marker:MapKitAnnotation = markerParser.parseMarkerPoint(markerPoint: markerPoint)
        mapView?.addAnnotation(marker)
    }
    
    func addMarkers(appendMarkers markerPoints: [MarkerPoint]){
        let markers:[MapKitAnnotation] = markerParser.parseMarkerPoints(markerPoints: markerPoints)
        mapView?.addAnnotations(markers)
    }
    
    func removeMarker(markerPoint :MarkerPoint){
        let marker:MapKitAnnotation = markerParser.parseMarkerPoint(markerPoint: markerPoint)
        mapView?.removeAnnotation(marker)
    }
    
    func removeAllMarkers(){
        if let allAnnotations = mapView?.annotations{
            mapView?.removeAnnotations(allAnnotations)
        }
    }
    
    
    func updateMarker(newMarker marker:MarkerPoint) {
        let annotation:MapKitAnnotation = markerParser.parseMarkerPoint(markerPoint: marker)
        annotation.coordinate = marker.coordinate
    }
    
}
