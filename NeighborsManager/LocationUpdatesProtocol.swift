//
//  LocationUpdatesUserMethodsDelegate.swift
//  GeoLocation
//
//  Created by LinuxPlus on 8/20/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation

protocol LocationUpdatesProtocol {
    var locationUpdatesDelegate: LocationUpdatesDelegate? {get set}
    func startUpdatingLocation()
    func stopUpdatingLocation()
}

extension LocationUpdatesProtocol{
    func stopUpdatingLocation(){}
}
