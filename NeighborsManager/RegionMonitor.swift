//
//  regionMonitor.swift
//  LocationMarkers
//
//  Created by LinuxPlus on 8/23/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import CoreLocation

class RegionMonitor: NSObject{
    
    var delegate: RegionMonitorDelegate?
    
    fileprivate var mapRegion: CLCircularRegion!
    fileprivate var mapRadius: CLLocationDistance!
    fileprivate let  markerParser = MarkerParser.shared
    
    init(center: CLLocationCoordinate2D, radius: CLLocationDistance) {
        super.init()
        mapRegion = getRegion(mapCenter: center, mapRadius: radius)
        mapRadius = radius
    }
}

extension RegionMonitor: RegionMonitorProtocol{
    
    func getRegion(mapCenter: CLLocationCoordinate2D, mapRadius: CLLocationDistance) -> CLCircularRegion {
        return CLCircularRegion(center: mapCenter, radius: mapRadius, identifier: "myRegion")
    }
    
    func updateCenter(newCenter center: CLLocationCoordinate2D) {
        mapRegion = getRegion(mapCenter: center, mapRadius: mapRadius)
        delegate?.regionMonitor(didUpdateRegion: mapRegion)
    }
    
    func isRegionContainsMarker(marker: MarkerPoint) -> Bool {
        let marker:MapKitAnnotation = markerParser.parseMarkerPoint(markerPoint: marker)
        return mapRegion.contains(marker.coordinate)
    }
    
    func getInsideRegionMarkers(markers: [MarkerPoint]) -> [MarkerPoint]{
        return markers.filter({ (marker) -> Bool in
            return isRegionContainsMarker(marker: marker)
        })
    }
    
    func updateMarkerState(inRegionMarkers: [MarkerPoint], marker: MarkerPoint) {
        let isInRegion = inRegionMarkers.contains(marker)
        if isInRegion{
            if isRegionContainsMarker(marker: marker){ //still in and exit
                delegate?.regionMonitor(stillInRegion: marker)
            }else{//exit region
                delegate?.regionMonitor(didExitRegion: marker)
            }
        }else{
            if isRegionContainsMarker(marker: marker){ //entered region
                delegate?.regionMonitor(didEnterRegion: marker)
            }
        }
    }
    
    
}
