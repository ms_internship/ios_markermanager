//
//  Annotation.swift
//  LocationMarkers
//
//  Created by LinuxPlus on 8/21/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class MarkerPoint: NSObject{
    
    var coordinate:CLLocationCoordinate2D!
    var title: String?
    var subtitle: String?
    var image: UIImage?
    var color: UIColor?
    var lastTimeUpdated: TimeInterval?
    var previousLocation: CLLocationCoordinate2D!
    
    init(coordinate:CLLocationCoordinate2D) {
        self.coordinate = coordinate
        previousLocation = coordinate
        self.lastTimeUpdated = Date().timeIntervalSince1970
    }
    
    func distanceMoved() -> CLLocationDistance{
        let oldLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        let currentLocation = CLLocation(latitude: previousLocation.latitude, longitude: previousLocation.longitude)
        return currentLocation.distance(from: oldLocation)
    }

}

