//
//  LocationManagerProtocol.swift
//  GeoLocation
//
//  Created by LinuxPlus on 8/20/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation

protocol LocationManagerProtocol {
    var delegate: LocationManagerDelegate? {get set}
    func isLocationServiceAuthorized() -> Bool
    func startTrackingUserLocation()
    func stopTrackingUserLocation()
    func requestAlwaysAuthorization()
    func requestWhenInUseAuthorization()
}


