//
//  ViewController.swift
//  LocationMarkers
//
//  Created by LinuxPlus on 8/21/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import UIKit
import MapKit
import Foundation
import CoreLocation

class ViewController: UIViewController{
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var newLatitude: UITextField!
    @IBOutlet weak var newLongitude: UITextField!
    @IBOutlet weak var markerIndex: UITextField!
    
    
    var markerManager:MarkerManagerProtocol!
    var locationManager:LocationManager!
    
    var markers :[MarkerPoint]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager = LocationManager()
        locationManager.delegate = self
        locationManager.startTrackingUserLocation()
        mapView.showsUserLocation = true
        
        let center = CLLocationCoordinate2D(latitude: 29.0001 , longitude: 30.0001)
        markers = Utiles.generateRandomMarkers(numberOfMarkers: 5)
        
        let markerBuilder = MarkerManagerBuilder { builder in
            builder.mapView = mapView
            builder.mapCenter = center
            builder.mapRadius = 150000
            builder.markers = markers
        }
        markerManager = MarkerManager(builder: markerBuilder)
        markerManager.delegate = self
        markerManager.setMarkers()
    }
    
    
    @IBAction func changePosition(_ sender: Any) {
        let newIndex =  Int(markerIndex.text!)
        let newlat =  Double(newLatitude.text!)
        let newlong =  Double(newLongitude.text!)
        let newCoordinate = CLLocationCoordinate2D(latitude: newlat!, longitude: newlong!)
        let markerPoint = markers[newIndex!]
        markerPoint.coordinate = newCoordinate
        markerManager.updateMarker(newMarker: markerPoint)
    }
}

extension ViewController: MarkerManagerDelegate{
    
    
    
    func markerManager(didEnterRegion marker: MarkerPoint) {
        let alert = Utiles.showAlert(title:"Region Entering", message: "\(marker.title ?? "someone") entered your region")
        print("\(marker.title ?? "someone") entered your region")
        present(alert, animated: true)
    }
    
    func markerManager(didExitRegion marker: MarkerPoint) {
        let alert = Utiles.showAlert(title:"Region Exit", message: "\(marker.title ?? "someone") exit your region")
        print("\(marker.title ?? "someone") exit your region")
        present(alert, animated: true)
    }
    
    func markerManager(markerTapped marker: MarkerPoint) {
        let alert = Utiles.showAlert(title: "Marker tapped", message: "marker name is : \(marker.title ?? "No Name")")
        present(alert, animated: true)
    }
}


extension ViewController: LocationManagerDelegate{
    
    func locationManager(didUpdateLocation location: CLLocation) {
        Utiles.displayMapWithRadius(mapView: mapView, mapCenter: location.coordinate, mapRadius: 150000)
        markerManager.updateCenter(newCenter: location)
    }
}

