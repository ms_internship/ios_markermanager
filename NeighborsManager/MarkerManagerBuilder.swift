//
//  MarkerManagerBuilder.swift
//  NeighborsManager
//
//  Created by LinuxPlus on 9/5/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import MapKit


class MarkerManagerBuilder{
    var mapView: MKMapView?
    var mapCenter:CLLocationCoordinate2D?
    var mapRadius:CLLocationDistance?
    var markers:[MarkerPoint]?
    var minimumDistance:CLLocationDistance?
    var minimumTime: Double?
    
    typealias BuilderClosure = (MarkerManagerBuilder) -> ()
    
    init(buildClosure: BuilderClosure) {
        buildClosure(self)
    }
}
