//
//  PointDrawerProtocol.swift
//  LocationMarkers
//
//  Created by LinuxPlus on 8/24/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import MapKit


protocol PointDrawerProtocol{
    var delegate: PointDrawerDelegate? {get set}
    
    func setMarkers(markerPoints: [MarkerPoint])
    func addMarker(appendMarker markerPoint: MarkerPoint)
    func addMarkers(appendMarkers markerPoints: [MarkerPoint])
    func removeAllMarkers()
    func removeMarker(markerPoint :MarkerPoint)
    func updateMarker(newMarker:MarkerPoint)
}
