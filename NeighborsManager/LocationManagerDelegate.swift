//
//  LocationManagerDelegate.swift
//  GeoLocation
//
//  Created by LinuxPlus on 8/20/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationManagerDelegate{
    func locationManager(didUpdateLocation location: CLLocation)
    func locationManager(didFailWithError error: Error)
    func locationAuthorizationFailed()
    func locationAuthorizationSuccesfull()
}

extension LocationManagerDelegate{
    func locationManager(didFailWithError error: Error){}
    func locationAuthorizationFailed(){}
    func locationAuthorizationSuccesfull(){}
}
