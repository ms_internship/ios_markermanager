//
//  RegionMonitorDelegate.swift
//  NeighborsManager
//
//  Created by LinuxPlus on 8/28/17.
//  Copyright © 2017 Allam. All rights reserved.
//

import Foundation
import CoreLocation

protocol RegionMonitorDelegate {
    func regionMonitor(didUpdateRegion region: CLCircularRegion)
    func regionMonitor(didEnterRegion marker: MarkerPoint)
    func regionMonitor(didExitRegion marker: MarkerPoint)
    func regionMonitor(stillInRegion marker: MarkerPoint)
}

extension RegionMonitorDelegate{
    func regionMonitor(didUpdateRegion region: CLCircularRegion){}
    func regionMonitor(didEnterRegion marker: MarkerPoint){}
    func regionMonitor(didExitRegion marker: MarkerPoint){}
    func regionMonitor(stillInRegion marker: MarkerPoint){}
}
